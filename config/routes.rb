Rails.application.routes.draw do
  devise_for :users, path: '', path_names: { sign_out: 'logout'}

  get 'welcome/index'
  post 'welcome/login'
  get 'welcome/logout'

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  resources :pessoa_juridicas
  resources :atrativos do
    post 'cadastrar_entradas'
  end
  resources :funcionarios do
    get 'toggleActiveStatus', on: :member
  end

end
