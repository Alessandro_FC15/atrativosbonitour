class FuncionariosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_funcionario, only: [:show, :edit, :update, :destroy, :toggleActiveStatus]
  before_action :check_admin_access

  # GET /funcionarios
  # GET /funcionarios.json
  def index
    puts 'index'

    @pessoa_juridica = current_user.pessoa_juridica

    @funcionarios = @pessoa_juridica.funcionarios
  end

  def toggleActiveStatus
    user = @funcionario.user
    user.active = !user.active
    user.save

    flash[:info] = "Usuário atualizado com sucesso."
    redirect_to action: 'index'
  end

  # GET /funcionarios/1
  # GET /funcionarios/1.json
  def show
  end

  # GET /funcionarios/new
  def new
    @funcionario = Funcionario.new
  end

  # GET /funcionarios/1/edit
  def edit
  end

  # POST /funcionarios
  def create
    @funcionario = Funcionario.new(funcionario_params)
    @funcionario.pessoa_juridica = current_user.pessoa_juridica
    @user = @funcionario.create_user(funcionario_user_params)

    respond_to do |format|
      if @funcionario.save
        @user = @funcionario.create_user(funcionario_user_params)
        @user.admin = false
        @user.active = true
        @user.pessoa_juridica = current_user.pessoa_juridica

        if @user.valid?
          @user.save

          @funcionario.user = @user
          @funcionario.save

          flash[:success] = "Cadastro realizado com sucesso."
          format.html { redirect_to funcionarios_path }
        else
          @funcionario.destroy
          format.html { render :new }
        end
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /funcionarios/1
  # PATCH/PUT /funcionarios/1.json
  def update
    respond_to do |format|
      if @funcionario.update(funcionario_params)
        format.html { redirect_to @funcionario, notice: 'Funcionario was successfully updated.' }
        format.json { render :show, status: :ok, location: @funcionario }
      else
        format.html { render :edit }
        format.json { render json: @funcionario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /funcionarios/1
  # DELETE /funcionarios/1.json
  def destroy
    @funcionario.destroy
    respond_to do |format|
      format.html { redirect_to funcionarios_url, notice: 'Funcionario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_funcionario
      @funcionario = Funcionario.find(params[:id])
    end

    def check_admin_access
      if not current_user.admin
        render :file => "public/401.html", :status => :unauthorized
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def funcionario_params
      params.require(:funcionario).permit(:nome, :cpf, :cargo)
    end

    def funcionario_user_params
      params.require(:funcionario).permit(:email, :password)
    end
end
