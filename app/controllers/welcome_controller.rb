class WelcomeController < ApplicationController
  def index
  end

  def logout
    sign_out

    render :destroy_user_session_path
  end

  def login
    @user = User.find_by_email(params[:email])

    if not @user
      flash[:error] = "E-mail não encontrado."
      render 'index'
    else
      if @user.valid_password?(params[:password])
        if @user.active
          sign_in(@user, scope: :user)

          # Redirecionar para área logada
          # redirect_to :controller => 'welcome', :action => 'index'
          redirect_to :controller => 'atrativos', :action => 'index'
        else
          flash[:error] = "A sua conta está desativada."
          render 'index'
        end
      else
        flash[:error] = "Senha incorreta."
        render 'index'
      end
    end
  end
end
