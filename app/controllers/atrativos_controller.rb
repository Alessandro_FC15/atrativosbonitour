class AtrativosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_atrativo, only: [:show, :edit, :update, :destroy]
  before_action :check_admin_access, only: [:new, :create, :destroy]

  # GET /atrativos
  def index
    @pessoa_juridica = current_user.pessoa_juridica
    @atrativos = @pessoa_juridica.atrativos
  end

  # GET /atrativos/1
  # GET /atrativos/1.json
  def show
  end

  # GET /atrativos/new
  def new
    @atrativo = Atrativo.new
  end

  # GET /atrativos/1/edit
  def edit
  end

  # POST /atrativos
  # POST /atrativos.json
  def create
    @atrativo = Atrativo.new(atrativo_params)
    @atrativo.pessoa_juridica = current_user.pessoa_juridica

    respond_to do |format|
      if @atrativo.save
        flash[:success] = 'Atrativo criado com sucesso!'
        format.html { redirect_to @atrativo }
        format.json { render :show, status: :created, location: @atrativo }
      else
        format.html { render :new }
        format.json { render json: @atrativo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /atrativos/1
  # PATCH/PUT /atrativos/1.json
  def update
    respond_to do |format|
      if @atrativo.update(atrativo_params)
        format.html { redirect_to @atrativo, notice: 'Atrativo was successfully updated.' }
        format.json { render :show, status: :ok, location: @atrativo }
      else
        format.html { render :edit }
        format.json { render json: @atrativo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /atrativos/1
  # DELETE /atrativos/1.json
  def destroy
    @atrativo.destroy
    respond_to do |format|
      flash[:info] = 'Atrativo removido com sucesso!'
      format.html { redirect_to atrativos_url}
      format.json { head :no_content }
    end
  end

  def cadastrar_entradas
    @atrativo = Atrativo.find(params[:atrativo_id])

    if not user_has_permission_to_access_atrativo(@atrativo, current_user)
      render :file => "public/401.html", :status => :unauthorized
    end

    registro_venda_ingressos = RegistroVendaIngressos.new
    registro_venda_ingressos.valorIngressoCriancaCentavos = @atrativo.valorIngressoCriancaCentavos
    registro_venda_ingressos.valorIngressoAdultoCentavos = @atrativo.valorIngressoAdultoCentavos
    registro_venda_ingressos.numeroAdultos = params[:numeroAdultos]
    registro_venda_ingressos.numeroCriancas = params[:numeroCriancas]
    registro_venda_ingressos.atrativo = @atrativo

    respond_to do |format|
      if registro_venda_ingressos.save
        flash[:success] = 'Venda de ingressos registrada com sucesso!'
      else
        flash[:error] = registro_venda_ingressos.errors.messages
      end

      format.html { redirect_to @atrativo }
    end
  end

  private
    def user_has_permission_to_access_atrativo(atrativo, user)
      return atrativo.pessoa_juridica_id == user.pessoa_juridica_id
    end

    def set_atrativo
      @atrativo = Atrativo.find(params[:id])

      if not user_has_permission_to_access_atrativo(@atrativo, current_user)
        render :file => "public/401.html", :status => :unauthorized
      end
    end

    def check_admin_access
      if not current_user.admin
        render :file => "public/401.html", :status => :unauthorized
      end
    end

    # Never trust parameters from the scary internet, only alvalorIngressoAdultolow the white list through.
    def atrativo_params
      params_atrativo = params.require(:atrativo).permit(:nome, :endereco, :tempoDuracaoMinutos, :capacidadeMaximaPessoas,
                                       :valorIngressoAdultoCentavos, :valorIngressoCriancaCentavos, :imagem)

      params_atrativo['valorIngressoAdultoCentavos'].gsub! ',', ''
      params_atrativo['valorIngressoCriancaCentavos'].gsub! ',', ''

      params_atrativo
    end
end
