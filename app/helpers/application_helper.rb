module ApplicationHelper
  def current_class_public_area?(test_path)
    if request.path == '/'
      return 'is-active' if test_path == '/welcome/index'
    else
      return 'is-active' if request.path == test_path
    end

    ''
  end

  def current_class_logged_area?(test_path)
    return (request.path.include? test_path) ? 'is-active' : ''
  end
end
