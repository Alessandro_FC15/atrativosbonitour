json.extract! pessoa_juridica, :id, :created_at, :updated_at
json.url pessoa_juridica_url(pessoa_juridica, format: :json)
