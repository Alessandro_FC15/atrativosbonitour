class RegistroVendaIngressos < ApplicationRecord
  belongs_to :atrativo

  validates :numeroAdultos, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
  validates :numeroCriancas, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
  validates :valorIngressoAdultoCentavos, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
  validates :valorIngressoCriancaCentavos, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
end
