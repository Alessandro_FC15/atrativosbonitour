class Funcionario < ApplicationRecord
  belongs_to :user
  belongs_to :pessoa_juridica

  validates :nome, presence: true
  validates :cpf, presence: true, length: { is: 14 }, uniqueness: true
  validates :cargo, presence: true
end
