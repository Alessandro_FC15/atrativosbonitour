class PessoaJuridica < ApplicationRecord
  has_one :user
  has_many :atrativos
  has_many :funcionarios

  # We will save the field 'cnpj' with the mask on it. So the length is 18 with the mask, and 14 without it.
  validates :cnpj, presence: true, length: { is: 18 }, uniqueness: true
  validates :razao_social, presence: true
  validates :nome_fantasia, presence: true
end
