class Atrativo < ApplicationRecord
  has_one_attached :imagem
  has_many :registro_venda_ingressos, :class_name => 'RegistroVendaIngressos'

  belongs_to :pessoa_juridica

  register_currency :brl
  monetize :valorIngressoAdultoCentavos, as: 'valor_ingresso_adulto', numericality: {
      greater_than_or_equal_to: 0}
  monetize :valorIngressoCriancaCentavos, as: 'valor_ingresso_crianca', numericality: {
      greater_than_or_equal_to: 0,
  }

  validates :nome, presence: true
  validates :endereco, presence: true
  validates :tempoDuracaoMinutos, presence: true
  validates :capacidadeMaximaPessoas, presence: true
  validates :valorIngressoAdultoCentavos, presence: true
  validates :valorIngressoCriancaCentavos, presence: true

  monetize :valor_total_arrecadado_centavos, as: 'valor_total_arrecadado'

  # scope :this_months_event, lambda { where("start_at >= ? AND start_at <= ?",
  #                                          Time.zone.now.beginning_of_month, Time.zone.now.end_of_month) }

  def get_registros_venda_ingressos_mes_atual
    self.registro_venda_ingressos.where("created_at >= ? and created_at <= ?",
                                        Date.current.beginning_of_month,
                                        Date.current.end_of_month)
  end

  def get_numero_ingressos_vendidos(somente_mes_atual = false)
    registros_venda_ingressos = somente_mes_atual ? self.get_registros_venda_ingressos_mes_atual : self.registro_venda_ingressos

    total_pessoas = 0

    for registro in registros_venda_ingressos
      total_pessoas += (registro.numeroCriancas + registro.numeroAdultos)
    end

    total_pessoas
  end

  def valor_total_arrecadado_centavos(somente_mes_atual = false)
    registros_venda_ingressos = somente_mes_atual ? self.get_registros_venda_ingressos_mes_atual : self.registro_venda_ingressos

    total_arrecadado = 0

    for registro in registros_venda_ingressos
      total_arrecadado += ((registro.numeroCriancas * registro.valorIngressoCriancaCentavos) + (registro.numeroAdultos * registro.valorIngressoAdultoCentavos))
    end

    total_arrecadado
  end
end
