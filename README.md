# Atrativos Bonitour

Plataforma desenvolvida em Ruby on Rails para que as pessoas possam cadastrar seus atrativos e assim torná-los visíveis para o mundo.

+ Disponível em: https://atrativos-bonitour.herokuapp.com/

## Instruções para execução local

A aplicação já se encontra disponível para testes no link cima. Caso vocẽ tenha o desejo de executar a aplicação localmente, uma vez clonado, os comandos são:

```shell
cd bin
bundle install
rake db:create
rake db:migrate
rails server
```
## Decisões de projeto

+ #### Diagrama UML

![Scheme](https://bitbucket.org/Alessandro_FC15/atrativosbonitour/raw/8f12b41445e8c6986400306179f1acc52b96f393/docs/diagramaUML.png)


+ #### Solução de autenticação de usuários

Para realizar a autenticação de usuários em Raisl, escolhi utilizar o **Devise**, por aparentemente ser a solução mais estável em Rails. No aplicação, existem 2 tipos de usuário. O usuário administrador da pessoa jurídica e os funcionários da pessoa jurídica. A fim de representar essa diferença, um atributo 'admin' foi adicionado à classe User.

Para representar o status das contas dos usuários, ativa ou desativada, utilizou-se um atributo 'active' na classe User.

+ #### Solução para salvar imagem dos atrativos

Para salvar a imagem relacionada à cada atrativo, escolhi utilizar o **Active Storage** do Rails mesmo, por facilita o upload de arquivos para um serviço de armazenamento na nuvem, como o Amazon S3, o Google Cloud Storage e por fazer a relação entre esses arquivos aos objetos da aplicação.

+ #### Utilização do 'money-rails' para os campos de dinheiro

Para os campos relacionados à dinheiro, utilizei a gem 'money-rails' visto que esta biblioteca fornece integração da gem 'money' com Rails.

+ #### Utilização do Material Design Lite

Optei por sair um pouco da minha zona de conforto nesse projeto, me afastando um pouco do Bootstrap e optei por experimentar o Material Design Lite. Achei que seria uma boa oportunidade para aprender um novo framework, além de que a aparência dos sites criados com o framework sempre me agradou.

## Possíveis Melhorias

+ #### Testes

Por conta do pouco tempo de desenvolvimento disponível, a aplicação não possui testes automatizados. Obviamente, em uma aplicação que realmente fosse para produção, a presença de testes seria indispensável. 

+ #### Interface para administradores

Uma interface web para os administradores dos sistema verificarem os registros cadastrados, desde as pessoas jurídicas até todos os atrativos e etc.
