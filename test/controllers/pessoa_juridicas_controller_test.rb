require 'test_helper'

class PessoaJuridicasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pessoa_juridica = pessoa_juridicas(:one)
  end

  test "should get index" do
    get pessoa_juridicas_url
    assert_response :success
  end

  test "should get new" do
    get new_pessoa_juridica_url
    assert_response :success
  end

  test "should create pessoa_juridica" do
    assert_difference('PessoaJuridica.count') do
      post pessoa_juridicas_url, params: { pessoa_juridica: {  } }
    end

    assert_redirected_to pessoa_juridica_url(PessoaJuridica.last)
  end

  test "should show pessoa_juridica" do
    get pessoa_juridica_url(@pessoa_juridica)
    assert_response :success
  end

  test "should get edit" do
    get edit_pessoa_juridica_url(@pessoa_juridica)
    assert_response :success
  end

  test "should update pessoa_juridica" do
    patch pessoa_juridica_url(@pessoa_juridica), params: { pessoa_juridica: {  } }
    assert_redirected_to pessoa_juridica_url(@pessoa_juridica)
  end

  test "should destroy pessoa_juridica" do
    assert_difference('PessoaJuridica.count', -1) do
      delete pessoa_juridica_url(@pessoa_juridica)
    end

    assert_redirected_to pessoa_juridicas_url
  end
end
