require 'test_helper'

class AtrativosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @atrativo = atrativos(:one)
  end

  test "should get index" do
    get atrativos_url
    assert_response :success
  end

  test "should get new" do
    get new_atrativo_url
    assert_response :success
  end

  test "should create atrativo" do
    assert_difference('Atrativo.count') do
      post atrativos_url, params: { atrativo: {  } }
    end

    assert_redirected_to atrativo_url(Atrativo.last)
  end

  test "should show atrativo" do
    get atrativo_url(@atrativo)
    assert_response :success
  end

  test "should get edit" do
    get edit_atrativo_url(@atrativo)
    assert_response :success
  end

  test "should update atrativo" do
    patch atrativo_url(@atrativo), params: { atrativo: {  } }
    assert_redirected_to atrativo_url(@atrativo)
  end

  test "should destroy atrativo" do
    assert_difference('Atrativo.count', -1) do
      delete atrativo_url(@atrativo)
    end

    assert_redirected_to atrativos_url
  end
end
