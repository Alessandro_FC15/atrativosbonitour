require "application_system_test_case"

class AtrativosTest < ApplicationSystemTestCase
  setup do
    @atrativo = atrativos(:one)
  end

  test "visiting the index" do
    visit atrativos_url
    assert_selector "h1", text: "Atrativos"
  end

  test "creating a Atrativo" do
    visit atrativos_url
    click_on "New Atrativo"

    click_on "Create Atrativo"

    assert_text "Atrativo was successfully created"
    click_on "Back"
  end

  test "updating a Atrativo" do
    visit atrativos_url
    click_on "Edit", match: :first

    click_on "Update Atrativo"

    assert_text "Atrativo was successfully updated"
    click_on "Back"
  end

  test "destroying a Atrativo" do
    visit atrativos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Atrativo was successfully destroyed"
  end
end
