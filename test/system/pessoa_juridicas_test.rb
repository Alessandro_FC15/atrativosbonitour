require "application_system_test_case"

class PessoaJuridicasTest < ApplicationSystemTestCase
  setup do
    @pessoa_juridica = pessoa_juridicas(:one)
  end

  test "visiting the index" do
    visit pessoa_juridicas_url
    assert_selector "h1", text: "Pessoa Juridicas"
  end

  test "creating a Pessoa juridica" do
    visit pessoa_juridicas_url
    click_on "New Pessoa Juridica"

    click_on "Create Pessoa juridica"

    assert_text "Pessoa juridica was successfully created"
    click_on "Back"
  end

  test "updating a Pessoa juridica" do
    visit pessoa_juridicas_url
    click_on "Edit", match: :first

    click_on "Update Pessoa juridica"

    assert_text "Pessoa juridica was successfully updated"
    click_on "Back"
  end

  test "destroying a Pessoa juridica" do
    visit pessoa_juridicas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pessoa juridica was successfully destroyed"
  end
end
