class RemoveValorIngressoCriancaFromAtrativos < ActiveRecord::Migration[5.2]
  def change
    remove_column :atrativos, :valorIngressoCrianca, :float
  end
end
