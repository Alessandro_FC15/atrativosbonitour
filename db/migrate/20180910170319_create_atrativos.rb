class CreateAtrativos < ActiveRecord::Migration[5.2]
  def change
    create_table :atrativos do |t|
      t.string :nome
      t.string :endereco
      t.integer :tempoDuracaoMinutos
      t.integer :capacidadeMaximaPessoas
      t.binary :imagem
      t.float :valorIngressoCrianca
      t.float :valorIngressoAdulto
      t.references :pessoa_juridica, foreign_key: true

      t.timestamps
    end
  end
end
