class AddPessoaJuridicaToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :pessoa_juridica, foreign_key: true
  end
end
