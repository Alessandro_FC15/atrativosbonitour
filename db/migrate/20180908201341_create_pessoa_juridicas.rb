class CreatePessoaJuridicas < ActiveRecord::Migration[5.2]
  def change
    create_table :pessoa_juridicas do |t|
      t.string :cnpj
      t.string :razao_social
      t.string :nome_fantasia
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
