class RemoveValorIngressoAdultoFromAtrativos < ActiveRecord::Migration[5.2]
  def change
    remove_column :atrativos, :valorIngressoAdulto, :float
  end
end
