class CreateFuncionarios < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionarios do |t|
      t.string :cpf
      t.string :nome
      t.string :cargo
      t.references :user, foreign_key: true
      t.references :pessoa_juridica, foreign_key: true

      t.timestamps
    end
  end
end
