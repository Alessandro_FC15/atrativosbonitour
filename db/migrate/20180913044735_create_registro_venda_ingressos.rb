class CreateRegistroVendaIngressos < ActiveRecord::Migration[5.2]
  def change
    create_table :registro_venda_ingressos do |t|
      t.integer :numeroAdultos
      t.integer :numeroCriancas
      t.integer :valorIngressoAdultoCentavos
      t.integer :valorIngressoCriancaCentavos
      t.references :atrativo, foreign_key: true

      t.timestamps
    end
  end
end
